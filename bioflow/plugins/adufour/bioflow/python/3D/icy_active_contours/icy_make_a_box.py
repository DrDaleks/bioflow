from plugins.adufour.quickhull import QuickHull3D
from plugins.adufour.roi.mesh.polygon import ROI3DPolygonalMesh
import os
from utils.path import make_sure_path_exists

def make_a_cuboid(seq, savdir):

    lx, ly, lz = seq.getPixelSizeX()*seq.getSizeX(), seq.getPixelSizeY()*seq.getSizeY(), seq.getPixelSizeZ()*seq.getSizeZ()
    bx, by, bz = 1.*lx/ly, 1., 1.*lz/ly

    l = [[0.,0.,0.], [1.,0.,0.], [0.,1.,0.], [1.,1.,0.], [0.,0.,1.], [1.,0.,1.], [0.,1.,1.], [1.,1.,1.]]

    contours = [[bx*x, by*y, bz*z] for [x,y,z] in l]
    flat_list = [item for sublist in contours for item in sublist]

    qhull = QuickHull3D(flat_list)
    qhull.triangulate()
    mesh = ROI3DPolygonalMesh(qhull)

    offm_path = os.path.join(savdir, "off_meshes")
    make_sure_path_exists(offm_path)
    times = seq.getSizeT()
    for t in range(times):
        flname = os.path.join(offm_path, "%dverfa.off" %(t))
        mesh.saveToOFF(flname)


    facets = [[3, 4, 2, 0], [3, 3, 6, 7], [3, 3, 0, 2], [3, 5, 6, 4],
              [3, 5, 0, 1], [3, 5, 3, 7], [3, 4, 6, 2], [3, 3, 2, 6],
              [3, 3, 1, 0], [3, 5, 7, 6], [3, 5, 4, 0], [3, 5, 1, 3]]

    return [contours]*times, [facets]*times
