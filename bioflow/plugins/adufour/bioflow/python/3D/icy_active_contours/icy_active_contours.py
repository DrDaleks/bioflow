from java.awt import Color
from java.awt import EventQueue
from java.lang import Runnable

from icy.main import Icy
from icy.image import IcyBufferedImage
from icy.sequence import Sequence, SequenceUtil
from icy.type import DataType

from plugins.adufour.activecontours import ActiveContours
from plugins.adufour.activecontours import SlidingWindow
from plugins.fab.trackmanager import TrackGroup
from plugins.fab.trackmanager import TrackManager

import os
from utils.path import make_sure_path_exists

def activeContours(seq, savdir, seg_param=None):
    if seg_param == None:
        seg_param = [4.7, 2.1, 2E-2]
    # first remove any trackgroup attached to that sequence
    for swimmingObject in Icy.getMainInterface().getSwimmingPool().getObjects(TrackGroup):
        trackGroup = swimmingObject.getObject()
        if (isinstance(trackGroup, TrackGroup)
            and trackGroup.getSequence() == seq):
            Icy.getMainInterface().getSwimmingPool().remove(swimmingObject)

    ac = ActiveContours()

    # the call to createUI and the modifications of the plugin parameters
    # have to be done from the main event thread, so they are encapsulated
    # in a Runnable called with EventQueue.invokeAndWait
    def run():

	ac.compute()

    	#ac.createUI()
        ac.input.setValue(seq)

	
        # regul_weight needs to be adjusted depending on resolution
        ac.regul_weight.setValue(0.1) #0.1)#0.01)#0.05*2*2)
# for roman part 0 = 0.1, for part 1 = 0.05, for part 2 = 0.05
# for beryl 20130703 CytoD08 = 0.1
        
        ac.edge_weight.setValue(0.)
        ac.region_weight.setValue(1.)

        ac.region_sensitivity.setValue(seg_param[0])
        
        ac.balloon_weight.setValue(0.)
        ac.axis_weight.setValue(0.)
        ac.coupling_flag.setValue(True)
        ac.evolution_bounds.setNoSequenceSelection()
        ac.contour_resolution.setValue(seg_param[1])#2.)
        #ac.contour_minArea.setValue(10) #unavailable since AC 2.6.0.0
        ac.contour_timeStep.setValue(0.05)#0.1) #0.01)#0.05)
        ac.convergence_winSize.setValue(200) #500)
        ac.convergence_operation.setValue(SlidingWindow.Operation.VAR_COEFF)
        ac.convergence_criterion.setValue(seg_param[2])
        #ac.output_rois.setValue(ac.ExportROI.ON_NEW_IMAGE)
        #ac.output_roiType.setValue(ac.ROIType.POLYGON)
        ac.tracking.setValue(True)
        #ac.evolution_bounds.getVariable().setValue(None)

        #ac.volume_constraint.setValue(True)
	#ac.showUI()

	
    EventQueue.invokeAndWait(run)
    
    print "Active contours start"
    ac.execute()
    print "Active contours finished"    

    

    # cleanup
    ac.getUI().close()

    #width = seq.getWidth()
    #height = seq.getHeight()
    #labelsSequence = Sequence()

    # find the trackGroup produced by ActiveContours
    for swimmingObject in Icy.getMainInterface().getSwimmingPool().getObjects(TrackGroup):
        candidateTrackGroup = swimmingObject.getObject()
        if (isinstance(candidateTrackGroup, TrackGroup)
            and candidateTrackGroup.getSequence() == seq):
            trackGroup = candidateTrackGroup

    trackGroup = ac.getTrackGroup()

    trackSegments = trackGroup.getTrackSegmentList()

    if len(trackSegments) > 1:
        print "Warning! The contour has divided itself during the tracking."
        print "We continue with the first contour (arbitrarily)."

    trackSegment = trackSegments[0]

    contours = []
    
    facets = []

    times = range(trackSegment.getDetectionList().size())

    
    ly = seq.getPixelSizeY()*seq.getSizeY() 
    offm_path = os.path.join(savdir, "off_meshes")
    make_sure_path_exists(offm_path)
    for t in times:
        contour = trackSegment.getDetectionAt(t)
        # SCALING AC OUTPUT
        # 3D AC is in microns, set Ly=1.:
        for point in contour.iterator():
            point.scale(1./ly)
        mesh = contour.toROI(ActiveContours.ROIType.POLYGON, None)
        mesh.optimizeVertexBuffer()
        # print the contours and facets to a file dolfin can read
        # otherwise I cannot seem to pass the facets over in the proper format

        flname = os.path.join(offm_path, "%dverfa.off" %(t))
        #flname = "%dverfa.off" %(t)
        mesh.saveToOFF(flname)
        faces = mesh.getCells()
        ##vertices = mesh.getVertices()

        # passing them over anyway
        # convert to a standard python list
        pyContour = [[point.x, point.y, point.z] for point in contour.iterator()]
        contours += [pyContour]

        pyFacets = [list(faces.get(i).vertexIndices) for i in range(0,faces.size())]
        facets += [pyFacets]
        
        ##vertices.get(200).position.x
        ##faces.get(0).vertexIndices[0]

        # also build a mask sequence
        #labelsImage = contourToLabelImage(contour, width, height)
        #labelsSequence.setImage(t, 0, labelsImage)

    print "Active contours post-processing finished: %d contour, 1st contour made of %d points" %(len(contours), len(contours[0]))

    return contours, facets#, labelsSequence

def contourToLabelImage(contour, width, height):      
    labelsIMG = IcyBufferedImage(width, height, 1, DataType.UBYTE)

    g = labelsIMG.createGraphics()
    g.setColor(Color(1))
    g.fill(contour.path)

    return labelsIMG


if __name__ == '__main__':
    seq = Icy.getMainInterface().getFocusedSequence()
    #contours, labelsSequence = activeContours(seq)
    #Icy.addSequence(labelsSequence)
    contours = activeContours(seq)
