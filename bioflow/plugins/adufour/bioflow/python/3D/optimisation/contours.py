from dolfin import *
import scipy.signal
import os # rename

# convert from a contour in pixel units to a list of vertices in dolfin units
def contour_to_vertices(contour, Dx, Dy, Dz, t, filedump=False):
    # if using actual vertices
    vertices = [Point(Dx*x, Dy*y, Dz*z) for [x, y, z] in contour]
    
    # if using file dumping
    # modify .off document to rescale vertices
    if filedump:
        old_file = open('%dverfa.off' %(t), 'r')
        new_file = open('%dverfax.off' %(t), 'w')
        new_file.write(old_file.readline())
        line = old_file.readline()
        nVertices=int(line.split()[0])
        new_file.write(line)
        print Dx, Dy, Dz
        for i in range(0, nVertices):
            l=old_file.readline().split()
            new_file.write('%.15f %.15f %.15f\n' %(Dx*float(l[0]), Dy*float(l[1]), Dz*float(l[2])))
            
            
        for l in old_file:
            new_file.write(l)
            
        os.rename('%dverfax.off' %(t), '%dverfa.off' %(t))
        
    return vertices

# resample a closed contour
def resample(factor, t, vertices=None):
    # if using actual vertices

    if vertices is not None:
        vertices_x = [vertex.x() for vertex in vertices]
        vertices_y = [vertex.y() for vertex in vertices]
        vertices_z = [vertex.z() for vertex in vertices]

        resampled_vertices_x = scipy.signal.resample(vertices_x, int(round(len(vertices_x)*factor)))
        resampled_vertices_y = scipy.signal.resample(vertices_y, int(round(len(vertices_y)*factor)))
        resampled_vertices_z = scipy.signal.resample(vertices_z, int(round(len(vertices_z)*factor)))

        resampled_vertices = []
        for x, y, z in zip(resampled_vertices_x, resampled_vertices_y, resampled_vertices_z):
            resampled_vertices.append(Point(x, y, z))
        return resampled_vertices
    # if using file dumping
    # modify .off document to resample
    else:
        old_file = open('%dverfa.off' %(t), 'r')
        new_file = open('%dverfaxx.off' %(t), 'w')
        new_file.write(old_file.readline())
        line = old_file.readline().split()
        nVertices=int(line[0])
        
        vertices_x = [0]*nVertices; vertices_y = [0]*nVertices; vertices_z = [0]*nVertices;
        for i in range(0, nVertices):
            l=old_file.readline().split()
            vertices_x[i] = float(l[0]); vertices_y[i] = float(l[1]); vertices_z[i] = float(l[2]);
        
        resampled_vertices_x = scipy.signal.resample(vertices_x, nVertices*factor)
        resampled_vertices_y = scipy.signal.resample(vertices_y, nVertices*factor)
        resampled_vertices_z = scipy.signal.resample(vertices_z, nVertices*factor)
            
        new_file.write('%d %d %d\n' %(len(resampled_vertices_x), int(line[1]), int(line[2])))
        for i in range(0, len(resampled_vertices_x)):
            new_file.write('%.15f %.15f %.15f\n' %(resampled_vertices_x[i], resampled_vertices_y[i], resampled_vertices_z[i]))
        
        for l in old_file:
            new_file.write(l)
            
if __name__ == '__main__':
    resample(0.05, 1., vertices=None)

