import os
import subprocess
import inspect


#Fenics_folder = "/Users/tlecomte/FEniCS-Dorsal-stable-20130828"
python_path = "/usr/bin/python"

def get_python_path():
	return python_path

def setup_environment():

	# FIXME when this is run several times, the evironment variables are polluted by the copies...

	# do the same as in .bash_profile for environment variables (DYLD_LIBRARY_PATH, PATH, BOOST_DIR, etc.)
	# they are added to the current environment
	# they will be propagated (as desired) to the child python process, so that Dolfin and VTK can be
	# properly loaded
	os.environ['DYLD_LIBRARY_PATH'] = '/usr/local/lib/vtk-5.10:' + os.environ.get('DYLD_LIBRARY_PATH', "")
	os.environ['PATH'] = '/usr/local/bin:/usr/local/sbin:' + os.environ.get('PATH', "")
	os.environ['BOOST_DIR'] = '/usr/local'
	try:
	    os.environ['PYTHONPATH'] = '/usr/local/lib/python'+':'+os.environ['PYTHONPATH']
	except KeyError:
	    os.environ['PYTHONPATH'] = '/usr/local/lib/python'

	# alternatively, I could run or parse HOME/.bash_profile
	
	# also replicate what is in Fenics_folder/share/dolfin/dolfin.conf
#	os.environ['DYLD_LIBRARY_PATH'] = os.path.join(Fenics_folder, "lib") + ':' + os.environ.get('DYLD_LIBRARY_PATH', "")
#	os.environ['PATH'] = os.path.join(Fenics_folder, "bin") + ':' + os.environ.get('PATH', "")
#	os.environ['PKG_CONFIG_PATH'] = os.path.join(Fenics_folder, "lib/pkgconfig") + ':' + os.environ.get('PKG_CONFIG_PATH', "")
#	os.environ['PYTHONPATH'] = os.path.join(Fenics_folder, "lib/python2.7/site-packages") + ':' + os.environ.get('PYTHONPATH', "")
#	os.environ['MANPATH'] = os.path.join(Fenics_folder, "share/man") + ':' + os.environ.get('MANPATH', "")
#	os.environ['DYLD_FRAMEWORK_PATH'] = '/opt/local/Library/Frameworks:' + os.environ.get('DYLD_FRAMEWORK_PATH', "")

	# alternatively, I could run or parse the Fenics conf file

def get_remote_addpaths_code():

#	fenics_dir = os.path.join(Fenics_folder, "lib/python2.7/site-packages")
	this_dir = os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))
	scripts_dir = os.path.join(this_dir, "..")

	add_paths_code = """
	import sys, os
	# add the dolfin python directory to the python path
	# subscripts path
	sys.path.append("%(scripts_dir)s")
	""" %{'scripts_dir': scripts_dir}
	
	return add_paths_code

if __name__ == "__main__":
	print "No code yet to test the module"

