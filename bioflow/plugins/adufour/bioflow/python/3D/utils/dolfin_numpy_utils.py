from dolfin import *
import numpy as np

#import scitools

#def to_array(mesh, a, Nx, Ny):
#    a2 = a if a.ufl_element().degree() == 1 else project(a, FunctionSpace(mesh, 'Lagrange', 1))
#    a_box = scitools.BoxField.dolfin_function2BoxField(a2, mesh, (Nx,Ny), uniform_mesh=True)
#    return a_box.values

# y axis is reversed in Dolfin compared to Numpy:
# 0 at bottom for Dolfin, at top for Numpy
def fix_ysign(a):
    return -a

def array_to_scalar(mesh, a):
    VI = FunctionSpace(mesh, "DG", 0)
    A = Function(VI)

    # convert to float32 (to force the right endianess)
    A.vector()[:] = a.flatten().repeat(6).astype(np.float32) # repeat because we have six tetrahedrons per pixel
#     a.flatten().repeat(2).astype(np.float32) # repeat because we have two triangles per pixel

    return A

def scalar_to_array(dest_mesh, Nx, Ny, Nz, v):
    Vu = FunctionSpace(dest_mesh, "DG", 0)

    a = project(v, Vu).vector().array()
    va = 1./6*(a[::6] + a[1::6] + a[2::6] + a[3::6] + a[4::6] + a[5::6]).reshape((Nz, Ny, Nx))

    return va

def arrays_to_vector(mesh, ax, ay, az):
    VgradI = VectorFunctionSpace(mesh, 'DG', 0)
    A = Function(VgradI)

#    ax = ax[::-1,::-1,::-1] ##change
#    ay = ay[::-1,::-1,::-1]
#    az = az[::-1,::-1,::-1]

    #ay = fix_ysign(ay)

    # convert to float32 (to force the right endianess)
    #A.vector()[::3] = ax.flatten().repeat(6).astype(np.float32) # repeat because we have six tetrahedrons per pixel
    #A.vector()[1::3] = ay.flatten().repeat(6).astype(np.float32) # repeat because we have six tetrahedrons per pixel
    #A.vector()[2::3] = az.flatten().repeat(6).astype(np.float32) # repeat because we have six tetrahedrons per pixel
    # in fenics 2016.2 you cannot get slices out of a function anymore [::2] so its either looping or using an aux like i am  doing now
    b = np.empty(VgradI.dim())
    b[::3] = ax.flatten().repeat(6).astype(np.float32)
    b[1::3] = ay.flatten().repeat(6).astype(np.float32)
    b[2::3] = az.flatten().repeat(6).astype(np.float32)
    A.vector()[:] = b

    return A

def vector_to_arrays(dest_mesh, Nx, Ny, Nz, v):
    Vu = FunctionSpace(dest_mesh, "DG", 0)

    vx, vy, vz = v.split()
    ax = project(vx, Vu).vector().array()
    vx = 1./6*(ax[::6] + ax[1::6] + ax[2::6] + ax[3::6] + ax[4::6] + ax[5::6]).reshape((Nz, Ny, Nx))
    ay = project(vy, Vu).vector().array()
    vy = 1./6*(ay[::6] + ay[1::6] + ay[2::6] + ay[3::6] + ay[4::6] + ay[5::6]).reshape((Nz, Ny, Nx))
    az = project(vz, Vu).vector().array()
    vz = 1./6*(az[::6] + az[1::6] + az[2::6] + az[3::6] + az[4::6] + az[5::6]).reshape((Nz, Ny, Nx))


    #vy = fix_ysign(vy)

    return vx, vy, vz
