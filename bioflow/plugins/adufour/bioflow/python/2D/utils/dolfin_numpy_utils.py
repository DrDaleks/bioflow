from dolfin import *
import numpy as np

#import scitools

#def to_array(mesh, a, Nx, Ny):
#    a2 = a if a.ufl_element().degree() == 1 else project(a, FunctionSpace(mesh, 'Lagrange', 1))
#    a_box = scitools.BoxField.dolfin_function2BoxField(a2, mesh, (Nx,Ny), uniform_mesh=True)
#    return a_box.values

# y axis is reversed in Dolfin compared to Numpy:
# 0 at bottom for Dolfin, at top for Numpy
def fix_ysign(a):
    return -a

# transforms an image in a matrix form into a dolfin function defined on a rectangular mesh.
# the mesh has the same number of square divisions than the image has pixels. As the dolfin function values
# are ordered in a vector fashion we just have to charge the pixel values repeating them as there are two triangles per square.
def array_to_scalar(mesh, a):
    VI = FunctionSpace(mesh, "DG", 0)
    A = Function(VI)

    #a=a[::-1,::-1]
    # convert to float32 (to force the right endianess)
    A.vector()[:] = a.flatten().repeat(2).astype(np.float32) # repeat because we have two triangles per pixel
#alsoworks    A.vector.set_local(a.flatten().repeat(2).astype(np.float32))
   
    return A

def scalar_to_array(dest_mesh, Nx, Ny, v):
    Vu = FunctionSpace(dest_mesh, "DG", 0)

    a = project(v, Vu).vector().array()
    va = 0.5*(a[::2] + a[1::2]).reshape((Ny, Nx))

    return va

# same thing as array to scalar but the two values of each vector are represented as consecutive values in the vector form
# of the dolphin function
def arrays_to_vector(mesh, ax, ay):
    VgradI = VectorFunctionSpace(mesh, "DG", 0)
    A = Function(VgradI)

    #ay=ay[::-1,::-1]
    #ax=ax[::-1,::-1]
    #ay = fix_ysign(ay)

    # convert to float32 (to force the right endianess)
#    # in fenics 2016.2 you cannot get slices out of a function anymore [::2] so its either looping or using an aux like i am  doing now
#    A.vector()[::2] = ax.flatten().repeat(2).astype(np.float32) # repeat because we have two triangles per pixel
#    A.vector()[1::2] = ay.flatten().repeat(2).astype(np.float32) # repeat because we have two triangles per pixel
    b = np.empty(VgradI.dim())
    b[::2] = ax.flatten().repeat(2).astype(np.float32)
    b[1::2] = ay.flatten().repeat(2).astype(np.float32)
    A.vector()[:] = b
    
    return A

def vector_to_arrays(dest_mesh, Nx, Ny, v):
    Vu = FunctionSpace(dest_mesh, "DG", 0)

    vx, vy = v.split()
    ax = project(vx, Vu).vector().array()
    vx = 0.5*(ax[::2] + ax[1::2]).reshape((Ny, Nx))
    ay = project(vy, Vu).vector().array()
    vy = 0.5*(ay[::2] + ay[1::2]).reshape((Ny, Nx))
    #vy = fix_ysign(vy)

    return vx, vy
