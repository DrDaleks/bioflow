import numpy as np
import matplotlib.pyplot as plt
from dolfin import *
from dolfin_adjoint import *

def flsolver(mesh, I1, I2, gradI2, f0, uw0, g0, r0, alpha, beta, gamma, zeta=1., tol=1e-4): #r0!


    adj_reset()
    # We define the discrete function spaces. A Taylor-Hood
    # finite-element pair is a suitable choice for the Stokes equations.
    # The control function is the Dirichlet boundary value on the velocity
    # field and is hence a function on the velocity space (note: FEniCS
    # cannot restrict functions to boundaries, hence the control is
    # defined over the entire domain).

    # sets up function spaces and functions
    Vel = VectorElement("CG", mesh.ufl_cell(), 2)  # Velocity
    Qes = FiniteElement("CG", mesh.ufl_cell(), 1)  # Pressure
    Yor = VectorElement("CG", mesh.ufl_cell(), 1)  # Force
    W = FunctionSpace(mesh, Vel*Qes)

    #V = FunctionSpace(mesh, Vel)
    #Q = FunctionSpace(mesh, Qes)
    V, Q = W.split()
    Y = FunctionSpace(mesh, Yor)

    v, q = TestFunctions(W)
    x = TrialFunction(W)
    u, p = split(x)
    s = Function(W, name="State")
    g = Function(V.collapse(), name="Control")
   
    f = Function(Y)
    r = Function(Q.collapse())  #r0!

    #print g0.vector()[1], g0.vector()[2], g0.vector()[3], g0.vector()[4], g0.vector()[5], g0.vector()[6], g0.vector()[7], g0.vector()[8]
    mesh.init()    
    n = FacetNormal(mesh)
    h = CellSize(mesh)
    
 # bc 2: u=0 on the boundary (we rewrote stokes to fulfill this U=u+g)
    def boundary(x, on_boundary):
            return on_boundary
    bcsWfor0 = [DirichletBC(W.sub(0), Constant((0.0, 0.0)), boundary)]
        
    # bc 3: p=0 (p=ct.) on the boundary (simulating constant pressure outside)
    bcp = [DirichletBC(W.sub(1), Constant(0.0), boundary)]

##0 Pressure pinpoint
#    boundaryMesh = BoundaryMesh(mesh, "exterior")
#    point0 = boundaryMesh.coordinates()[0]
#    pin_x0 = point0[0]
#    pin_x1 = point0[1]
#    class PinPoint(SubDomain):
#        def inside(self, x, on_boundary):
#            return abs(x[0] - pin_x0) < DOLFIN_EPS and abs(x[1] - pin_x1) < DOLFIN_EPS

#    pinPoint = PinPoint()
#    pinVal = Constant(0.0)
#    bcp = [DirichletBC(W.sub(1), pinVal, pinPoint, "pointwise")]
        
    # union of bcs
    bcs = bcsWfor0 + bcp
        
    
    # CONSTANTS
    mu=1. # viscosity (DIMENSIONLESS) placeholder
    At=1. # time step (DIMENSIONLESS) placeholder

    # Weak formulation of the  Stokes equation with the velocity split as u+g so as bc(u)=0. Pressure sign reversed for convenience.
#    a = (mu*inner(nabla_grad(u), nabla_grad(v)) + p*nabla_div(v) + (nabla_div(u))*q)*dx
#    L = (inner(f, v) - mu*inner(nabla_grad(g), nabla_grad(v)) - nabla_div(g)*q+r*q)*dx
    a = (mu*inner(grad(u), grad(v)) + inner(p, div(v)) + inner(q, div(u)))*dx
    L = (inner(f, v) - mu*inner(grad(g), grad(v)) - inner(q, div(g)) + r*q)*dx    
        
    # Next we assemble and solve the system once to record it with
    # :py:mod:`dolin-adjoint`.
    A, b = assemble_system(a, L, bcs)
    solve(A, s.vector(), b, 'mumps')
    u, p = split(s)

    #*pow(r,2)*dx
    #inner(nabla_grad(r), nabla_grad(r))
    
    # define the function to be minimized #g5.50.1.1.1
    J = Functional(
                +  5.*zeta*pow(r,2)*dx
                + 30./2.*At*pow(inner(gradI2, u + g - uw0 ) + I2 - I1, 2)*dx
                + 5.*gamma*(inner((nabla_grad(g[0]) - inner(nabla_grad(g[0]), n)*n), (nabla_grad(g[0]) - inner(nabla_grad(g[0]), n)*n)) + inner((nabla_grad(g[1]) - inner(nabla_grad(g[1]), n)*n), (nabla_grad(g[1]) - inner(nabla_grad(g[1]), n)*n)))*ds
                + alpha*1./2.*inner(f, f)*dx
                + beta*.1/2.*inner(nabla_grad(f), nabla_grad(f))*dx

                ) #r0!
                
    # define the control functions
    m1 = Control(g, g0) #Control(g, g0)
    m2 = Control(f, f0) #Control(f, f0)
    m3 = Control(r, r0) #r0!
    Jhat = ReducedFunctional(J, [m1, m2, m3]) #r0!

    # by default, :py:func:`minimize` uses the L-BFGS-B
    # algorithm. Run the minimization algorithm.
    print 'start minimizing'
    g_opt, f_opt, r_opt = minimize(Jhat, tol=tol) #r0!
    print 'ended minimization'

    # get the resulting mesh-discrete functions out of the minimization
    g.assign(g_opt)
    f.assign(f_opt)
    r.assign(r_opt) #r0!
    
    # solve the system to get the other mesh-discrete variables
    A, b = assemble_system(a, L, bcs)
    solve(A, s.vector(), b, 'mumps', annotate=False)
    u, p = split(s) #fenics 2016 needs deepcopy to project?
   
    # change pressure sign
    return project(u+g,V.collapse(), annotate=False), -1.*project(p, Q.collapse(), annotate=False), project(g, V.collapse(), annotate=False), project(f, Y, annotate=False), project(r,Q.collapse(), annotate=False) #r0!
