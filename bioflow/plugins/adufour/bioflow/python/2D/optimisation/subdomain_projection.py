from dolfin import *
import numpy as np
from matplotlib.path import Path
from utils.dolfin_numpy_utils import fix_ysign#, reorder
from utils.dolfin_numpy_utils import vector_to_arrays, scalar_to_array

# Sub domain for the interior
class InteriorDomain(SubDomain):
    def __init__(self, vertices):
        SubDomain.__init__(self)
        npVertices = np.array([[p.x(), p.y()] for p in vertices])
        self.path = Path(npVertices)

    def inside(self, x, on_boundary):
        return self.path.contains_point(x)

# this function projects a function onto a restriction of a mesh
def project_inside(I, destMesh, Nx, Ny, vertices, outside_value = None):
    interiorDomain = InteriorDomain(vertices)

    # Initialize mesh function for interior domains
    domains = CellFunction("size_t", destMesh)
    domains.set_all(0)
    interiorDomain.mark(domains, 1)

    #plot(domains)

    V = FunctionSpace(destMesh, "DG", 0)

    u = TrialFunction(V)
    v = TestFunction(V)

    if outside_value == None:
        data = I.vector().array()
        # the outside value is smaller that the min of the data
        outside_value = min(data) - (max(data) - min(data))/10.

    Iout = Constant(outside_value)

    # Define new measures associated with the interior domains
    dx = Measure("dx")[domains]

    # Define variational form
    # Ip = I in the triangle, Ip = 0 outside
    a = u*v*dx(1) + u*v*dx(0)
    L = I*v*dx(1) + Iout*v*dx(0)
    A, b = assemble_system(a, L)

    Is = Function(V)
    solve(A, Is.vector(), b)

    isx = scalar_to_array(destMesh, Nx, Ny, Is)

    return Is, isx

def vector_project_inside(f, destMesh, Nx, Ny, vertices, outside_value = 0.):
    interiorDomain = InteriorDomain(vertices)

    # Initialize mesh function for interior domains
    domains = CellFunction("size_t", destMesh)
    domains.set_all(0)
    interiorDomain.mark(domains, 1)

    #plot(domains)

    V = VectorFunctionSpace(destMesh, "DG", 0)

    u = TrialFunction(V)
    v = TestFunction(V)

    if outside_value == None:
        data = f.vector().array()
        # the outside value is smaller that the min of the data
        outside_value = min(data) - (max(data) - min(data))/10.

    fout = Constant((outside_value,outside_value))

    # Define new measures associated with the interior domains
    dx = Measure("dx")[domains]

    # Define variational form
    # Ip = I in the triangle, Ip = 0 outside
    a = inner(u,v)*dx(1) + inner(u,v)*dx(0)
    L = inner(f,v)*dx(1) + inner(fout,v)*dx(0)
    A, b = assemble_system(a, L)

    fs = Function(V)
    solve(A, fs.vector(), b)
    
    fx, fy = vector_to_arrays(destMesh, Nx, Ny, fs)

    return fs, fx, fy
