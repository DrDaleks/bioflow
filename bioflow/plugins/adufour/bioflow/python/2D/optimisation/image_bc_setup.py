from dolfin import *
import numpy as np

from utils.imageDerivatives import image_first_derivatives
from utils.dolfin_numpy_utils import array_to_scalar, arrays_to_vector
from utils.pyramid import downscale_single, downsample_single
from utils.warp import warp_coords, warp, outside_indices

from stokessolverOOP import flsolver
#from stokessolver import flsolver
#from stokescompressiblesolver import flsolver

# Prepares the image mesh, scales the images, prepares the image derivatives, transforms them from a matrix to a dolfin function and projects them
# sets up the boundary conditions (away from dolfin-adjoint) and calls the solver
def multiresolution_algorithm(meshes, areas, im1, im2, ratio, alpha, beta, gamma, zeta, tol, silent=True, ug0=None):
    
    # image dimensions in PIXELS
    Ny, Nx = im1.shape

    Ly = 1e0
    Dy = Ly/Ny

    Dx = ratio[0]*Dy
    Lx = Nx*Dx
    
    for i, (mesh, area) in enumerate(zip(meshes[::-1], areas[::-1])): #loop from coarsest to finest scale 
    
        n = mesh.num_cells()
        print "Scale %d/%d (%d cells, corresponding image size approx. = %dx%d)" %(i+1, len(meshes), n, np.sqrt(n/2.), np.sqrt(n/2.))

        downscale_factor = np.sqrt(2*area/(Dx*Dy))
        if not silent:
            print float(mesh.hmax())/Dx, mesh.hmax(), Dx, 1./2.*float(mesh.hmax() + mesh.hmin())/2./Dx, np.sqrt(area)/Dx

        if False:

            im1_down = downscale_single(im1, downscale_factor)
            im2_down = downscale_single(im2, downscale_factor)
            print "Image downscaled"

            dxI2, dyI2 = image_first_derivatives(im2_down)
            dxI2 *= 1./Dx # convert to image units (Lx, Ly), instead of pixels
            dyI2 *= 1./Dy
            print "Derivatives computed"

            im_mesh = RectangleMesh (Point(0., 0.), Point(Lx, Ly), Nx, Ny)
            #print "image mesh created"

            if mesh<>meshes[-1]:

                coords, x_warp, y_warp = warp_coords(im_mesh, Nx, Ny, u, Lx, Ly, coord=True, silent=silent)
                im2_down = warp(im2_down, coords)
                dxI2 = warp(dxI2, coords)
                dyI2 = warp(dyI2, coords)

                 # where the warping goes outside the image domain, cancel the data-attachment term
                out_ind = outside_indices(x_warp, y_warp, Nx, Ny)
                dxI2[out_ind] = 0.
                dyI2[out_ind] = 0.
                im2_down[out_ind] = im1_down[out_ind] # so that It = 0
                print "Image warped"

        else:

            im1_down = downsample_single(im1, downscale_factor)
            im2_down = downsample_single(im2, downscale_factor)
            print "Image downscaled"
            Ny_H, Nx_H = im1_down.shape
            Dx_H = Lx/Nx_H
            Dy_H = Ly/Ny_H
            
            dxI2, dyI2 = image_first_derivatives(im2_down)
            dxI2 *= 1./Dx_H # convert to image units (Lx, Ly), instead of pixels
            dyI2 *= 1./Dy_H
            print "Derivatives computed"

            im_mesh = RectangleMesh (Point(0., 0.), Point(Lx, Ly), Nx_H, Ny_H)
            #print "image mesh created

            if mesh<>meshes[-1]:

                coords, x_warp, y_warp = warp_coords(im_mesh, Nx_H, Ny_H, u, Lx, Ly, coord=True, silent=silent)
                im2_down = warp(im2_down, coords)
                dxI2 = warp(dxI2, coords)
                dyI2 = warp(dyI2, coords)

                 # where the warping goes outside the image domain, cancel the data-attachment term
                out_ind = outside_indices(x_warp, y_warp, Nx_H, Ny_H)
                dxI2[out_ind] = 0.
                dyI2[out_ind] = 0.
                im2_down[out_ind] = im1_down[out_ind] # so that It = 0
                print "Image warped"
                
        # convert the image represented in a matrix to a 0 degree function in dolfin (1 ct value per element)
        I1 = array_to_scalar(im_mesh, im1_down)
        I2 = array_to_scalar(im_mesh, im2_down)
        gradI2 = arrays_to_vector(im_mesh, dxI2, dyI2)
    
        # PROJECT the images on the image rectangle mesh to the cell mesh
        IFSel = FiniteElement("CG", mesh.ufl_cell(), 1); IFS = FunctionSpace(mesh, IFSel)
        IGFSel = VectorElement("CG", mesh.ufl_cell(), 1); IGFS = FunctionSpace(mesh, IGFSel)
        I1 = project(I1, IFS)
        I2 = project(I2, IFS)
        gradI2 = project(gradI2, IGFS)
        print "Image and Image_grad projected"
    
        # the regularization parameters have to be scaled too
        alpha_s = alpha*downscale_factor
        beta_s = beta*downscale_factor
        gamma_s = gamma*downscale_factor
        zeta_s = zeta*downscale_factor   
      
        # project previous guess to new guess
        Vel = VectorElement("CG", mesh.ufl_cell(), 2); V = FunctionSpace(mesh, Vel)  # Velocity
        Yor = VectorElement("CG", mesh.ufl_cell(), 1); Y = FunctionSpace(mesh, Yor)  # Force
        Qes = FiniteElement("CG", mesh.ufl_cell(), 1); Q = FunctionSpace(mesh, Qes)  #r0! Out-of-plane flow
        if mesh==meshes[-1]:
            f0 = Function(Y)
            uw0 = Function(V)
            g0 = Function(V)
            #f0.vector()[:] = 0. # functions are already initialized to 0
            #uw0.vector()[:] = 0.
            #g0.vector()[:] = 0.
            r0 = Function(Q)    #r0!
            print "Initialized guess"        
        else:
            f0 = project(f, Y)
            uw0 = project(u, V)
            g0 = project(g, V)
            r0 = project(r, Q) #r0!
            print "Projected previous guess"
        
        # bc 1: pinpoint a boundary spot to 0 pressure so solving is possible
        #boundaryMesh = BoundaryMesh(mesh, "exterior")
        #point0 = boundaryMesh.coordinates()[0]
        #pin_x0 = point0[0]
        #pin_x1 = point0[1]

        #eps = 1e-10
        #class PinPoint(SubDomain):
        #    def inside(self, x, on_boundary):
        #        return abs(x[0] - pin_x0) < DOLFIN_EPS and abs(x[1] - pin_x1) < DOLFIN_EPS

        #pinPoint = PinPoint()
        #pinVal = Constant(0.0)
        #bcp = [DirichletBC(W.sub(1), pinVal, pinPoint, "pointwise")]


       
        ### RUN solver algorithm
        #### get the previous result to be taken in account by the solver (dolfin should let you pass parameters to the scipy it uses to optimize, but it is much of a mess because those cannot be ofc dolfin functions)
        u, p, g, f, r = flsolver(mesh, I1, I2, gradI2, f0, uw0, g0, r0, alpha_s, beta_s, gamma_s, zeta=zeta_s, tol=tol) #r0! r, r0
    #plot(u, interactive=True)#; plot(u); interactive();
    print 'Solved'
    return u, p, f, g, mesh, I1, I2, gradI2, r #, d, w, r, I1, I2, gradI2, uw0, mesh

