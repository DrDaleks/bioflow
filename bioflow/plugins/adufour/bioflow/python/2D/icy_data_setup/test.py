import execnet, sys, os, inspect

# modify the python path to find sub-scripts
sys.path.append(os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None))))

import environment_setup
from icyexecnetgateway import IcyExecnetGateway

environment_setup.setup_environment()
python_path = environment_setup.get_python_path()
add_paths_code = environment_setup.get_remote_addpaths_code()

group = execnet.Group()
group.defaultspec = "popen//python=" + python_path

for i in range(4):  # 4 CPUs
    group.makegateway()


def process_item(channel):
    # task processor, sits on each CPU
    import time
    import random
    channel.send("ready")
    for x in channel:
        if x is None:  # we can shutdown
            break
        # sleep random time, send result
        time.sleep(3)
        channel.send("done")

# execute taskprocessor everywhere
mch = group.remote_exec(process_item)

# get a queue that gives us results
q = mch.make_receive_queue(endmarker=-1)
tasks = range(10)  # a list of tasks, here just integers

print "Queue size: " + str(len(mch))

terminated = 0

while 1:
    
    channel, item = q.get()
    if item == -1:
        terminated += 1
        print "terminated %s" % channel.gateway.id
        print "Number of terminated tasks: " + str(terminated)
        if terminated == len(mch):
            print "got all results, terminating"
            break
        continue
    if item != "ready":
        print "other side %s returned %r" % (channel.gateway.id, item)
    if not tasks:
        print "no tasks remain, sending termination request to all"
        mch.send_each(None)
        tasks = -1
    if tasks and tasks != -1:
        task = tasks.pop()
        channel.send(task)
        print "sent task %r to %s" % (task, channel.gateway.id)

group.terminate()
