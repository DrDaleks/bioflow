package plugins.adufour.bioflow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.swing.JSeparator;

import icy.file.FileUtil;
import icy.file.Loader;
import icy.gui.frame.progress.AnnounceFrame;
import icy.network.NetworkUtil;
import icy.system.IcyHandledException;
import icy.system.SystemUtil;
import icy.system.thread.Processor;
import icy.util.ClassUtil;
import icy.util.ZipUtil;
import plugins.adufour.docker.DockerUtil;
import plugins.adufour.ezplug.EzButton;
import plugins.adufour.ezplug.EzPlug;
import plugins.tprovoost.scripteditor.gui.ScriptingEditor;

public class BioFlow extends EzPlug
{
    /** The temporary folder where the plug-in and resources are stored */
    private static File tmpFolder;
    
    /**
     * @return The base folder where BioFlow is unpacked (typically, /tmp/BioFlow on Linux)
     */
    public static String getBaseFolder()
    {
        return tmpFolder.getPath();
    }
    
    @Override
    protected void execute()
    {
        // Won't be necessary
    }
    
    @Override
    protected void initialize()
    {
        AnnounceFrame progress = new AnnounceFrame("Loading BioFlow...");
        
        try
        {
            checkFor3rdPartySoftware();
            
            tmpFolder = new File(SystemUtil.getTempDirectory() + "/BioFlow");
            
            // House-keeping (just in case)
            if (tmpFolder.exists()) FileUtil.delete(tmpFolder, true);
            
            if (SystemUtil.isMac() && isDockerUsed())
            {
                // For security reasons, Docker on Mac won't allow /var to be bound directly
                // Don't worry, /var is actually /private/var, and /private can be bound
                tmpFolder = new File("/private" + tmpFolder);
            }
            
            // Running via Eclipse?
            if (getClass().getResource("") != null)
            {
                // Use Eclipse's target folder
                File eclipseTarget = new File(getClass().getResource("").getFile()).getParentFile().getParentFile().getParentFile();
                FileUtil.copy(eclipseTarget, tmpFolder, true, true);
            }
            else
            {
                // Unpack the JAR file
                String jarPath = FileUtil.getApplicationDirectory() + "/" + getDescriptor().getJarFilename();
                ZipUtil.extract(jarPath, tmpFolder.getPath());
            }
            
            String packageName = ClassUtil.getPackageName(getClass().getName());
            tmpFolder = new File(tmpFolder.getPath() + "/" + packageName.replace('.', '/'));
            FileUtil.createDir(tmpFolder + "/output");
            
            // No run button
            getUI().setActionPanelVisible(false);
            
            addEzComponent(new EzButton("Open README (offline PDF)", new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    NetworkUtil.openBrowser(new File(tmpFolder.getPath() + "/doc/README.pdf").toURI());
                }
            }));
            addEzComponent(new EzButton("Open publication (online PDF)", new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    NetworkUtil.openBrowser("http://rdcu.be/vet8");
                }
            }));
            
            addComponent(new JSeparator(JSeparator.HORIZONTAL));
            
            addEzComponent(new EzButton("Open BioFlow 2D", new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    loadScript(new File(tmpFolder.getPath() + "/python/2D/BioFlow2D.py"));
                }
            }));
            addEzComponent(new EzButton("Open 2D sample", new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    loadSample(new File(tmpFolder.getPath() + "/python/2D/sample2D.tif"));
                }
            }));
            
            addComponent(new JSeparator(JSeparator.HORIZONTAL));
            
            addEzComponent(new EzButton("Open BioFlow 3D", new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    loadScript(new File(tmpFolder.getPath() + "/python/3D/BioFlow3D.py"));
                }
            }));
            addEzComponent(new EzButton("Open 3D sample", new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    loadSample(new File(tmpFolder.getPath() + "/python/3D/sample3D.tif"));
                }
            }));
            
        }
        catch (Throwable error)
        {
            String errorMessage = "Couldn't initialize the plug-in.\n";
            errorMessage += "Check that Dolfin is properly installed (see the plug-in documentation)\n";
            errorMessage += "\nA longer error message was sent to the output console\n";
            errorMessage += "If problems persist, please ask for help in the Icy forum";
            
            error.printStackTrace();
            
            throw new IcyHandledException(errorMessage);
        }
        finally
        {
            progress.close();
        }
    }
    
    protected void loadScript(File scriptFile)
    {
        ScriptingEditor editor = new ScriptingEditor();
        editor.addToDesktopPane();
        editor.setVisible(true);
        try
        {
            editor.openFile(scriptFile);
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }
    }
    
    protected void loadSample(final File sampleFile)
    {
        new Processor(1).submit(new Runnable()
        {
            public void run()
            {
                addSequence(Loader.loadSequence(sampleFile.getPath(), 0, true));
            }
        });
    }
    
    private static void checkFor3rdPartySoftware() throws FileNotFoundException, InterruptedException, IOException
    {
        // Check for dolfin first
        if (isDolfinInstalled(true)) return;
        
        // If absent, try Docker instead
        if (isDockerInstalled(true)) return;
        
        // If none is present, fail miserably
        String errorMessage = "Couldn't find 3rd party software.\n";
        errorMessage += "Please ensure that either Dolfin-Adjoint or Docker are installed\n";
        errorMessage += "(See the online documentation for more information)";
        throw new IcyHandledException(errorMessage);
    }
    
    /**
     * @return whether Dolfin is installed on the local system
     */
    public static boolean isDolfinInstalled()
    {
        return isDolfinInstalled(false);
    }
    
    /**
     * @param verbose
     *            Set to <code>true</code> to print the version found in the console
     * @return whether Dolfin is installed on the local system
     */
    public static boolean isDolfinInstalled(boolean verbose)
    {
        return searchFor("Dolfin", "dolfin-version", verbose);
    }
    
    /**
     * @return whether Docker is installed on the local system
     */
    public static boolean isDockerInstalled()
    {
        return isDockerInstalled(false);
    }
    
    /**
     * @param verbose
     *            Set to <code>true</code> to print the version found in the console
     * @return whether Docker is installed on the local system
     */
    public static boolean isDockerInstalled(boolean verbose)
    {
        return searchFor("Docker", "docker --version", verbose);
    }
    
    private static boolean searchFor(String title, String command, boolean verbose)
    {
        if (verbose) System.out.print("[BioFlow] Searching for " + title + "...");
        
        if (SystemUtil.isMac())
        {
            // Patch the path variable (needed _at least_ by macOS 10.12)
            command = "/usr/local/bin/" + command;
        }
        
        try
        {
            Process process = Runtime.getRuntime().exec(command);
            BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (process.waitFor() == 0)
            {
                if (verbose) System.out.println(" found (" + outputReader.readLine() + ")");
                return true;
            }
        }
        catch (Exception e)
        {
        }
        
        if (verbose) System.out.println(" not found");
        return false;
    }
    
    private static boolean isDockerUsed()
    {
        return isDockerInstalled() && !isDolfinInstalled();
    }
    
    @Override
    public void clean()
    {
        
    }
    
    /**
     * Runs BioFlow through a Docker instance. This method creates a command-line call to docker
     * that shares the folder where BioFlow is unpacked, then launches the
     * <code>communication_setup.py</code> script (in the <code>icy_data_setup</code> folder)
     * 
     * @param dstFolder
     *            the folder where the results should be copied over from the Docker instance
     */
    public static void runViaDocker2D(String dstFolder)
    {
        runViaDocker("python/2D/", dstFolder);
    }
    
    /**
     * Runs BioFlow through a Docker instance. This method creates a command-line call to docker
     * that shares the folder where BioFlow is unpacked, then launches the
     * <code>communication_setup.py</code> script (in the <code>icy_data_setup</code> folder)
     * 
     * @param dstFolder
     *            the folder where the results should be copied over from the Docker instance
     */
    public static void runViaDocker3D(String dstFolder)
    {
        runViaDocker("python/3D/", dstFolder);
    }
    
    /**
     * Runs BioFlow through a Docker instance. This method creates a command-line call to docker
     * that shares the folder where BioFlow is unpacked, then launches the
     * <code>communication_setup.py</code> script (in the <code>icy_data_setup</code> folder)
     * 
     * @param dstFolder
     *            the folder where the results should be copied over from the Docker instance
     */
    private static void runViaDocker(final String sourcePath, final String dstFolder)
    {
        try
        {
            String dolfinImage = "quay.io/dolfinadjoint/dolfin-adjoint:latest";
            
            HashMap<String, String> folderBindings = new HashMap<String, String>();
            folderBindings.put(tmpFolder.getPath(), "/home/fenics/shared");
            
            String dolfin = DockerUtil.startContainer(dolfinImage, folderBindings, "/home/fenics/shared/" + sourcePath);
            
            System.out.println("Installing Excenet...");
            DockerUtil.runCommand(dolfin, "sudo -H pip install execnet");
            
            System.out.println("Running BioFlow...");
            DockerUtil.runCommand(dolfin, "python icy_data_setup/communication_setup.py");
            
            // Once the results are in, move them to their final destination
            FileUtil.copy(tmpFolder.getPath() + "/output", dstFolder, false, true);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
    
}
