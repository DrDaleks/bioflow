package plugins.adufour.bioflow;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.io.File;
import java.lang.reflect.Array;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import icy.canvas.IcyCanvas;
import icy.file.FileUtil;
import icy.image.colormap.IcyColorMap;
import icy.image.colormap.JETColorMap;
import icy.painter.Overlay;
import icy.plugin.interface_.PluginBundled;
import icy.sequence.Sequence;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarFolder;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.Var;
import vtk.vtkUnstructuredGrid;
import vtk.vtkXMLUnstructuredGridReader;

public class BioFlowDisplay extends EzPlug implements PluginBundled
{
    EzVarFolder resultFolder = new EzVarFolder("Result folder", null);
    EzVarSequence seq = new EzVarSequence("Display on");
    
    EzVarDouble f_scaleFactor = new EzVarDouble("Scale factor", 1, 1, 100, 1);
    EzVarDouble f_minRange = new EzVarDouble("Min. range");
    EzVarDouble f_maxRange = new EzVarDouble("Max. range");
    Point3d[][] f_positions;
    Vector3d[][] f_vectors;
    
    EzVarDouble u_scaleFactor = new EzVarDouble("Scale factor", 1, 1, 100, 1);
    EzVarDouble u_minRange = new EzVarDouble("Min. range");
    EzVarDouble u_maxRange = new EzVarDouble("Max. range");
    Point3d[][] u_positions;
    Vector3d[][] u_vectors;
    
    EzVarDouble p_scaleFactor = new EzVarDouble("Scale factor", 1, 1, 100, 1);
    EzVarDouble p_minRange = new EzVarDouble("Min. range");
    EzVarDouble p_maxRange = new EzVarDouble("Max. range");
    Point3d[][] p_positions;
    double[][] p_values;
    
    @Override
    protected void initialize()
    {
        addEzComponent(resultFolder);
        addEzComponent(seq);
        
        EzGroup force = new EzGroup("Force", f_scaleFactor, f_minRange, f_maxRange);
        addEzComponent(force);
        
        EzGroup speed = new EzGroup("Velocity", u_scaleFactor, u_minRange, u_maxRange);
        addEzComponent(speed);
        
        EzGroup pressure = new EzGroup("Pressure", p_scaleFactor, p_minRange, p_maxRange);
        addEzComponent(pressure);
    }
    
    @Override
    protected void execute()
    {
        final Sequence s = seq.getValue(true);
        
        // Remove the previous overlay
        for (Overlay overlay : s.getOverlaySet())
            if (overlay.getName().startsWith("BioFlow")) overlay.remove();
        
        File folder = resultFolder.getValue(true);
        File pressure_folder = new File(folder.getPath() + "/p/p_pvd");
        File force_folder = new File(folder.getPath() + "/f/f_pvd");
        File speed_folder = new File(folder.getPath() + "/u/u_pvd");
        
        // All folders are assumed to have the same number of files (obviously...)
        int nbFiles = FileUtil.getFiles(force_folder.getPath(), "vtu", true, false).length;
        
        p_positions = new Point3d[nbFiles][];
        p_values = new double[nbFiles][];
        
        f_positions = new Point3d[nbFiles][];
        f_vectors = new Vector3d[nbFiles][];
        
        u_positions = new Point3d[nbFiles][];
        u_vectors = new Vector3d[nbFiles][];
        
        read(pressure_folder, p_positions, p_values, p_minRange, p_maxRange);
        s.addOverlay(new BioFlowOverlay("[BioFlow] Pressure", p_positions, p_values, p_minRange.getVariable(), p_maxRange.getVariable(), p_scaleFactor.getVariable()));
        
        read(force_folder, f_positions, f_vectors, f_minRange, f_maxRange);
        s.addOverlay(new BioFlowOverlay("[BioFlow] Force field", f_positions, f_vectors, f_minRange.getVariable(), f_maxRange.getVariable(), f_scaleFactor.getVariable()));
        
        read(speed_folder, u_positions, u_vectors, u_minRange, u_maxRange);
        s.addOverlay(new BioFlowOverlay("[BioFlow] Velocity field", u_positions, u_vectors, u_minRange.getVariable(), u_maxRange.getVariable(), u_scaleFactor.getVariable()));
        
    }
    
    private static class BioFlowOverlay extends Overlay
    {
        final Point3d[][] points;
        final Object[] data;
        final Var<Double> minRange, maxRange, scaleFactor;
        final IcyColorMap colorMap = new JETColorMap();
        
        public BioFlowOverlay(String name, Point3d[][] points, double[][] scalarData, Var<Double> minRange, Var<Double> maxRange, Var<Double> scaleFactor)
        {
            super(name, OverlayPriority.TEXT_TOP);
            this.points = points;
            this.data = scalarData;
            this.minRange = minRange;
            this.maxRange = maxRange;
            this.scaleFactor = scaleFactor;
        }
        
        public BioFlowOverlay(String name, Point3d[][] points, Vector3d[][] vectorData, Var<Double> minRange, Var<Double> maxRange, Var<Double> scaleFactor)
        {
            super(name, OverlayPriority.TEXT_TOP);
            this.points = points;
            this.data = vectorData;
            this.minRange = minRange;
            this.maxRange = maxRange;
            this.scaleFactor = scaleFactor;
        }
        
        @Override
        public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
        {
            Graphics2D g2 = (Graphics2D) g.create();
            float stroke = (float) canvas.canvasToImageLogDeltaX(3);
            g2.setStroke(new BasicStroke(stroke));
            
            int currentT = canvas.getPositionT();
            if (currentT == sequence.getSizeT() - 1) return;
            
            Object array = data[currentT];
            
            int nArrows = Array.getLength(array);
            
            // The data is scaled to the image height
            int height = sequence.getHeight();
            
            Point3d p1 = new Point3d(), p2 = new Point3d();
            
            double max_min = maxRange.getValue() - minRange.getValue();
            for (int i = 0; i < nArrows; i++)
            {
                double value = 1;
                if (array instanceof double[])
                {
                    value = ((double[]) array)[i];
                }
                else if (array instanceof Vector3d[])
                {
                    value = ((Vector3d[]) array)[i].length();
                }
                
                // Color
                float colorIndex = (float) ((value - minRange.getValue()) / max_min);
                if (colorIndex > 1f) colorIndex = 1f;
                float red = colorMap.getNormalizedRed(colorIndex);
                float green = colorMap.getNormalizedGreen(colorIndex);
                float blue = colorMap.getNormalizedBlue(colorIndex);
                g2.setColor(new Color(red, green, blue));
                
                p1.scale(height, points[currentT][i]);
                
                if (array instanceof double[])
                {
                    g2.fill(new Ellipse2D.Double(p1.x - stroke, p1.y - stroke, stroke * 2, stroke * 2));
                }
                else if (array instanceof Vector3d[])
                {
                    p2.scaleAdd(scaleFactor.getValue(), (Vector3d) Array.get(array, i), p1);
                    g2.draw(new Line2D.Double(p1.x, p1.y, p2.x, p2.y));
                    g2.fill(new Ellipse2D.Double(p2.x - stroke, p2.y - stroke, stroke * 2, stroke * 2));
                }
            }
            
            g2.dispose();
        }
    }
    
    private static void read(File folder, Point3d[][] positions, Vector3d[][] vectors, EzVarDouble minRange, EzVarDouble maxRange)
    {
        vtkXMLUnstructuredGridReader vtuReader = new vtkXMLUnstructuredGridReader();
        double min = Double.MAX_VALUE, max = 0.0;
        
        for (String path : FileUtil.getFiles(folder.getPath(), "vtu", true, false))
        {
            vtuReader.SetFileName(path);
            vtuReader.Update();
            vtkUnstructuredGrid grid = vtuReader.GetOutput();
            final int nArrows = grid.GetNumberOfPoints();
            
            int t = Integer.parseInt(path.substring(path.lastIndexOf(File.separator) + 1).substring(3, 6));
            positions[t] = new Point3d[nArrows];
            vectors[t] = new Vector3d[nArrows];
            
            for (int i = 0; i < nArrows; i++)
            {
                positions[t][i] = new Point3d(grid.GetPoints().GetPoint(i));
                Vector3d vec = new Vector3d(grid.GetPointData().GetVectors().GetTuple3(i));
                vectors[t][i] = vec;
                double len = vec.length();
                if (len < min) min = len;
                if (len > max) max = len;
            }
        }
        
        // Set the data range
        minRange.setValue(min);
        maxRange.setValue(max);
    }
    
    private static void read(File folder, Point3d[][] positions, double[][] values, EzVarDouble minRange, EzVarDouble maxRange)
    {
        vtkXMLUnstructuredGridReader vtuReader = new vtkXMLUnstructuredGridReader();
        double min = Double.MAX_VALUE, max = 0.0;
        
        for (String path : FileUtil.getFiles(folder.getPath(), "vtu", true, false))
        {
            vtuReader.SetFileName(path);
            vtuReader.Update();
            vtkUnstructuredGrid grid = vtuReader.GetOutput();
            final int nArrows = grid.GetNumberOfPoints();
            
            int t = Integer.parseInt(path.substring(path.lastIndexOf(File.separator) + 1).substring(3, 6));
            positions[t] = new Point3d[nArrows];
            values[t] = new double[nArrows];
            
            for (int i = 0; i < nArrows; i++)
            {
                positions[t][i] = new Point3d(grid.GetPoints().GetPoint(i));
                double value = grid.GetPointData().GetScalars().GetTuple1(i);
                if (value < min) min = value;
                if (value > max) max = value;
                values[t][i] = value;
            }
        }
        
        // Set the data range
        minRange.setValue(min);
        maxRange.setValue(max);
    }
    
    @Override
    public void clean()
    {
        
    }

    @Override
    public String getMainPluginClassName()
    {
        return BioFlow.class.getName();
    }
}
