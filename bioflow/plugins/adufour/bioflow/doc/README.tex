\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{caption}
\usepackage[T1]{fontenc}
\usepackage{url}
\usepackage{vmargin}
\usepackage{url}
\usepackage{listings}
\usepackage{comment}


\def\u{\vec{u}}
\def\v{\vec{v}}

\def\n{{\nabla}}
\def\f{\vec{f}}
\def\g{\vec{g}}
\def\r{\vec{r}}
\def\Jdata{J_{\mbox{data}}}
\def\Jreg{J_{\mbox{reg}}}
\def\dx{\overrightarrow{dx}}
\def\Eh{\emph{Entamoeba histolytica}~}

\setmarginsrb{2cm}{2cm}{2cm}{2cm}{0pt}{0pt}{0pt}{1cm}
\usepackage{amsmath,cases}
\usepackage{hyperref}

\title{BioFlow - README}
\author{Aleix Boquet Pujadas}

\date{}

\begin{document}
\maketitle


\section{Description}

This Icy plug-in offers a tool capable of extracting internal cell pressure, velocity and forces from 2D and 3D image data sequences. The full method can be found in the paper\cite{biof} and the accompanying Supplementary Material. Briefly, it consists in tracking the cell over time while minimising a given functional describing its motion constrained to the weak formulation of a customised Stokes model (using a Finite Element Method and a variational data assimilation framework). That is, solving 
\begin{equation}\label{eq:bioflow2d}
\u,p,\f,\g,r = \mbox{argmin} \left( \Jdata(\u  \Delta t) + \Jreg(\f, \g, r) \right) \mbox{,  subject to } A(\u,p,\f,\g,r)=0
\end{equation}
\begin{equation*}\label{reg1}
\left\{
\begin{array}{rcl}
\Jdata(\u  \Delta t) & = &  \int_\Omega \left( \n I_2 \cdot \u  \Delta t + (I_2-I_1) \right)^2 d\Omega, \\
\Jreg(\f,\g,r) & = & \alpha \int_{\Omega} \|\f\|^2 d\Omega + \gamma \oint_{\Gamma} \|\nabla_{\Gamma} \g\|^2 d\Gamma + \eta \int_{\Omega} \|\n r\|^2 d\Omega,\end{array}
\right.
\end{equation*}
where the model (in this case Stokes) is expressed in $A$:
\begin{numcases}{A(\u,p,\f,\g, r)=}
  \nabla p - \mu \nabla^2 \u - \f & \text{in $\Omega$} \nonumber \\  
 \nabla \cdot \u -r & \text{in $\Omega$} \label{eq:stokesOOPF} \\
 \u -\g & \text{on $\Gamma$.} \nonumber
\end{numcases}
In fact the equations are solved in a non-dimensional form and a multi-scale approach is used for the functional $J_{data}$. All of this is covered in detail in the Supplementary Material.
\section{Introduction}
After loading a video-microscopy sequence into the software, an initial contour is defined around the cell on the first frame (this manual step can be automated using any available segmentation technique). The main BioFlow script (written in Jython) is launched from Icy’s script editor and starts by segmenting and tracking the cell boundary over time using an active contour method, then creates a multi-scale version of the image and contour for each frame. The contour-image pairs are subsequently transferred to a python script running the FEniCS Finite Element software, where the remainder of the computations take place: (a) the image and contour are converted into a Finite Element mesh using the Mshr (mesh generation) module; (b) the PDE system in 3D or 2D is then solved using the MUMPS solver; (c) finally, the functional J is minimised using the dolfin-adjoint module. This process is iterated until convergence and repeated from coarse to fine image scales using appropriate warping. To reduce computation time, each pair of frames in the video sequence is processed in parallel using all available processors on the workstation. The final results and figures were produced either within Icy or with the ParaView software. Notice that the results will depend on the accuracy of the model, in our case we are observing small filaments suspended within the cytoplasm and therefore our images do move like fluid.

\section{Quick start guide}

\begin{itemize}

	\item Ensure Fenics (\url{www.fenics.org}) and dolfint-adjoint (\url{www.dolfin-adjoint.org}) are installed and can properly interface with Python. Or use the virtual machine provided. Or install docker.
	\item Open the Icy software and import the image sequence you are going to work with. If the resolution is too high you may want to scale it down, and if the background is useless you may want to crop it (so that multiscale does not spend time warping it at each step).
	\item Draw a ROI over the cell of interest on the first frame (or use the HK-Means plugin if the sequence is 3D).
	\item Install and launch the plug-in by typing {\em Biophysical Optical Flow} in Icy's search bar. This will install all source codes in a temporary folder on the disk and open the main script within Icy's {\em Script Editor}.
	\item Adjust the various options (output folder, plotting the results, parameters etc.) and click on the {\em Play} icon.
	\item If the code converges, the results will be saved in the output folder.
    
	\item There are two options to visualize the results:\\
    \textbf{Option 1: Icy} a) Load a video that was analyzed with BioFlow; b) Launch the {\em BioFlowDisplay} plug-in via Icy's search bar; c) Select the top-level folder (named with time and date) containing the results for this video; c) Click the {\em Play} button to load and display the pressure, velocity and force fields directly onto the raw data as extra layers; d) Go the the {\em layer} panel (in Icy's left-hand control panel) to show or hide the various layers; e) The scale factor and min-max ranges can be used to improve visualization; f) The camera icons on top of the sequence viewer can be used to save a screenshots of the final display.\\
    \textbf{Option 2: Paraview} a) Right-click in the Pipeline browser and click {\em open}; b) Select any of the top-level \verb|.pvd| file (e.g. \verb|u.pvd| for the velocity field, not \verb|u_t000.pvd|), then click {\em apply}; c) If the \verb|.pvd| file contains a vector field (e.g. velocity or force fields), add the {\em glyph} filter to visualize the arrows; d) when working with 3D data, the {\em slice} filter may come in handy.
	
\end{itemize}

\section{Parameters}

\begin{itemize}
\item \textbf{savdir}: output folder (needs not exist). \textit{string}. Example: "\verb|~/Documents/Bioflow/example|"

\item \textbf{parallel}: number of processors to be used (each pair of images in a sequence is parallelized). \textit{integer} or None (as in not parallel).

\item \textbf{plotting}: whether or not you want a pop-up plot of the velocity field. \textit{boolean}.

\item \textbf{phys param}: physical parameters of the problem, in this case the viscosity constant in $Pa \ s$. If unknown, use $[1.]$. In any case, the pressure and force will only be up to a constant and the velocity and divergence will be unaffected. \textit{[float]}

\item \textbf{fun param}: the parameters/constants in the adapted optical flow functional. $\alpha$, $\gamma$, $\eta$ are associated to the penalisation of body forces, tangential velocity and gradient of divergence, respectively. $tol$ is the convergence tolerance of the algorithm (with respect to 10e-4). \textit{[$\alpha$, 1., $\gamma$, $\eta$, tol]}. In 3D that is \textit{[$\alpha$, 1., $\gamma$, tol]} In our case, these parameters are optimised using Brent to try to predict the next image.

\item \textbf{scale param}: the algorithm normally uses the metadata of the image (e.g. time between frames) to scale the problem to a unitless version, solve it and rescale it back. If the image does not inherently contain this information provide it as:
\textit{[time between frames, length of the y size of the image, [pixel size x/pixel size y]]} in seconds, microns and unitless ratio (e.g. micron divided by micron). For the 3D case: \textit{[time between frames, length of the y size of the image, [pixel size x/pixel size y, pixel size z/pixel size y]]}

\item \textbf{segment}: True if you want to run on the cell, False if you want BioFlow to run on the whole image. \textit{boolean}.

\item \textbf{segmentation param}: in order to segment the cell in the given image the algorithm uses the active contours model. Some of the parameters associated with the method include \textit{[region sensitivity, contour resolution, convergence criterion]}. The default parameters (\textit{$[1.5, 2., 1E-2]$}) should work, but if the user detects any faulty segmentation, they can adjust. Decreasing all the parameters (but tol) will give more strength to the actual fitting of the movement and may fix issues.  

\item \textbf{multiscale param}: in order to be able to detect large movements, the algorithm follows a multi-scale approach; first it detects the bigger displacements in a smoothed image and then progressively refines them until the original resolution. \textit{[min pixel size, max pixel size, scale factor, mesh resolution]}. Respectively, the coarsest scale to be considered (the number of pixels that the image should have so that the displacements are one pixel or less); the finest scale (\textit{None} for original image, smaller for less detailed results); the pixel ratio between two consecutive scales; the mesh resolution at the original scale (default is up to pixel size).
\end{itemize}

\section{Output}
The results are stored in the folder chosen as output. This folder contains a folder for each of the quantities $u$ (velocity), $p$ (pressure), $r$ (divergence), mesh... Each of this folders contains a \textit{.vtu} file for each time point storing the values of the quantity at each mesh point (and the mesh itself). In this folder the user can also find a \textit{.pvd} both for each time point and for the whole sequence at one (e.g. \textit{u.pvd}). \textit{.vtu} contains the real data and \textit{.pvd} are “pointers” to it. \textit{u.pvd} will contain the whole sequence so you can play it like a movie while \textit{u\_t003.pvd} (e.g.) will contain the flow between images 3 and 4.

These extensions are one of VTK's file formats and can be read and imported to C++, Java, Python, etc. scripts through easily available packages after which you can play handle the data. Furthermore, they are accepted by many visualisation tools such as Paraview or Icy itself.  One needs only to open the corresponding \textit{.pvd} file (if the magnitude is vectorial the Glyph filter has also to be applied) of a time point or of the whole sequence.

The data folder will also contain a \textit{aux.txt} file for each time point in the following format:

\noindent \textit{timepoint maxu minu avgu maxf minf avgf maxp minp avgp}

\noindent so that they are readily accessible without a VTK import. These can be concatenated through a bash script and easily imported as text and plotted anywhere (e.g. R, Python, Excel...):

\begin{lstlisting}
#! /bin/bash
SAVEIFS=$IFS
IFS=$(echo -en "\n\b") # otherwise spaces in folder names mess everything up
folder_list=`ls -d */  | cut -f1 -d'/'` # getting rid of the slash. OR echo */ , in case: sed 's/$/data'`
for f in ${folder_list};
do
    cat ${f}/data/t* > ${f}_results_ufp.txt; 
done
IFS=$SAVEIFS # OR unset IFS if by default

\end{lstlisting}
in case you have many folders. Otherwise just:
\begin{lstlisting}
cat *.txt > whatever_name.txt
\end{lstlisting}

\begin{comment}
\section*{Workflow}
The code's workflow is depicted in figure \ref{fig}, and is decomposed into the following routines:

\begin{figure}[h]
	\centering 
	\includegraphics[scale=0.43]{Slide1.jpg} 
	\caption{\label{fig}General workflow of the program.}
\end{figure}

\paragraph{\fontfamily{lmtt}\selectfont BiophysicalOpticalFlow2D()}
  
\begin{itemize}
	\item Starts up the Icy Interface and gets the images sequence.
	\item Offers different options such as silencing or plotting.
\end{itemize}

\paragraph{\fontfamily{lmtt}\selectfont optimizer(seq, silent=False, savdir=None, parallel=True, plotting=True, automatic=False)}

\begin{itemize}
	\item Checks and transforms, if necessary, the image matrix values to double.
	\item Gets the contour on each image through {\fontfamily{lmtt}\selectfont icy$\_$active$\_$contours.py}.
	\item Creates the list of images to process.
	\item Creates a folder path to store the results.
	\item Packs the images to be processed.
	\item Gathers the data into image-pairs to be analysed separately. Change [args[i]] to args to run all the pairs.
	\item Carries on with {\fontfamily{lmtt}\selectfont communication$\_$setup()}.
\end{itemize}

\paragraph{\fontfamily{lmtt}\selectfont  icy$\_$active$\_$contours.py}

\begin{verbatim}
	activeContours(seq):
\end{verbatim}	

Calls the {\em Active Convours} plug-in with the indicated parameters. NB: a higher contour resolution can be obtained by adjusting the line \verb|ac.contour_resolution.setValue(3.0)| (replacing 3.0 by a value in pixels corresponding to the average distance between neighbour points of the contour).

\paragraph{\fontfamily{lmtt}\selectfont communication$\_$setup.py}

\begin{verbatim}
	process_tasks(tasks, parallel=True):
\end{verbatim}	

(for each image pair) Unpacks the images and sets up the communication channel to connect the Jython layer to the native python code and libraries.

\paragraph{\fontfamily{lmtt}\selectfont meshsetup$\_$resultdumping()}

\begin{verbatim}
	main((contour, im1, im2, savdir, t, silent, plotting)):
\end{verbatim}	

\begin{itemize}
	\item Receives/calculates the number of pixels, the image length (scales it to length=1 in the y direction) and the length-per-pixel.
	\item Normalises the image according to the mean.
	\item Sets up the penalisation weights.
	\item Converts the contour, in pixel units, to a list of vertices in Dolfin format and length units through {\fontfamily{lmtt}\selectfont contours()}.
	\item Generates the mesh through {\fontfamily{lmtt}\selectfont meshPyramid()}.
	\item Chooses the mesh of the pyramid that is closest to the given scale (change if desired)
	\item Carries on with {\fontfamily{lmtt}\selectfont image$\_$bc$\_$setup()}.
	\item Handles the results and prints them in the appropriate folders.
\end{itemize}

\paragraph{\fontfamily{lmtt}\selectfont  meshPyramid()}

Creates a list of fine-to-coarse cell meshes. Cell$\_$size can be changed to make it finer or coarser.

\paragraph{\fontfamily{lmtt}\selectfont contours()}

\begin{verbatim}
	contour_to_vertices(contour, Dx, Dy):
\end{verbatim}	

Converts the contour, in pixel units, to a list of vertices in Point-dolfin-format and length units.

\begin{verbatim}
	resample(vertices, factor):
\end{verbatim}	
Resamples(Fourier) the contours to other resolutions.

\paragraph{\fontfamily{lmtt}\selectfont image$\_$bc$\_$setup()}

\begin{verbatim}
	multiresolution_algorithm(meshes, areas, im1, im2, alpha, beta, gamma, zeta, tol, scale, silent=False, f_only=False, ug0=None):\end{verbatim}	

\begin{itemize}

	\item Receives/calculates the number of pixels, the image length (scales it to length=1 in the y direction) and the length-per-pixel.
	\item Downscales the images to an appropriate resolution for the mesh. 
	\item Calculates the image derivative through {\fontfamily{lmtt}\selectfont imageDerivatives()} and scales them according to the pixel length.
	\item Creates a mesh for the image.
	\item Converts the images (represented in matrix form) to a 0 degree function in dolfin (1 ct value per element) through {\fontfamily{lmtt}\selectfont dolfin$\_$numpy$\_$utils()}.
	\item Projects the images on the image rectangle mesh to the cell mesh.
	\item Scales the regularization parameters accordingly.
	\item Carries on with {\fontfamily{lmtt}\selectfont stokessolverOOPF()}.

\end{itemize}

\paragraph{\fontfamily{lmtt}\selectfont imageDerivatives()}

\begin{verbatim}
	activeContours(seq):
\end{verbatim}	

Uses finite differences to apply a kernel to the image matrix so that each element is substituted by its derivative. By transposing the matrix the same kernel can be applied to get the y derivatives.

\paragraph{\fontfamily{lmtt}\selectfont dolfin$\_$numpy$\_$utils()}

\begin{verbatim}
	array_to_scalar(mesh, a):
\end{verbatim}	
Transforms an image in matrix form into a dolfin function defined on a rectangular mesh. The mesh has the same number of square divisions as the image has pixels. Since the dolfin function values are ordered in a vector fashion we just have to charge the pixel values repeating them as there are two triangles per square.

\begin{verbatim}
	arrays_to_vector(mesh, ax, ay):
\end{verbatim}	

Similar to array$\_$to$\_$scalar but the two values of each vector are represented as consecutive values in the vector form of the dolfin function.

\paragraph{\fontfamily{lmtt}\selectfont stokessolverOOPF()}

\begin{verbatim}
	flsolver(mesh, bcs, I1, I2, gradI2, alpha, beta, gamma, zeta=0., tol=1e-4):
\end{verbatim}	

\begin{itemize}
	\item Sets up the weak formulation of the model (compressible Stokes equation) with the velocity split as u+g and the pressure inverted.
    \item Sets up the boundary conditions. As we separate $u=u_0+g$ one condition is $u_0=0$ in the boundary. Another boundary condition is either to (1) pinpoint a boundary pressure value to 0 (for example) to help the solver. Or (2) set the boundary pressure to 0 to imitate constant pressure outside, it does not matter much anyway because the pressure can jump as there is no pressure gradient restriction (we do not minimize it).
	\item Assembles the system with the boundary conditions and solves it once for dolfin-adjoint to generate the adjoint model.
	\item Define the J function to be minimized through the PDE-optimization dolfin solver.
	\item Define the control functions (f, g) that act as variables for the minimization.
	\item Minimize and obtain the control functions (f, g).
	\item Solve for (u, p) and change p sign.
	\item Return the results.
\end{itemize}
\end{comment}
%\bibliographystyle{plain}
%\bibliography{bflowminimal}
\begin{thebibliography}{9}

   \bibitem{biof}
          Boquet-Pujadas, A., Lecomte, T., Manich, M., Thibeaux, R., Labruy{\`e}re, E., Guill{\'e}n, N., Olivo-Marin, J.C. \& Dufour, A. C.
          BioFlow: a non-invasive, image-based method to measure speed, pressure and forces inside living cells.
          \emph{Scientific reports} \textbf{7}, 9178, \href{https://www.nature.com/articles/s41598-017-09240-y}{doi:10.1038/s41598-017-09240-y}
          (2017).

\end{thebibliography}

\end{document}